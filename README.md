# Requisitos de software
* terraform >=0.13.3
* python >=3.8.5
* ansible >=2.9.13
* docker >=19.03.12
* docker-py
* google-cloud-sdk

# Pre-requisitos
* Para Terraform se está usando una caraterística de dependencia de módulos que se ha añadido recientemente, por lo cual se necesita que esté en su versión más actual
* Ansible está usando docker-py para el manejo de contenedores, si no se tiene instalado, el script fallará
* Se está tomando en cuenta que el usuario tiene permisos para ejecutar los comandos docker

# Aclaración
* Al comienzo del script se pide la contraseña ya que utiliza permisos root para inicializar el servicio de Docker en caso que este no esté activo por defecto.
* Aunque se ha parametrizado las región, zona y los nombres, para un uso práctico del mismo script solamente se le pedirá el ***Project ID***, **la acción a realizar**, **el archivo de configuración en JSON de GCP** y **la contraseña** para ejecutar comandos de root

# Implementación
## Create
![Diagram](./files/other/create_diagram.png)
[![asciicast](./files/other/image.png)](https://asciinema.org/a/360550)

## Destroy
[![asciicast](./files/other/image.png)](https://asciinema.org/a/360549)
## Output
[![asciicast](./files/other/image.png)](https://asciinema.org/a/360518)
