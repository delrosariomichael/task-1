#!/bin/bash
echo Ingrese el Project ID
read PROJECTID
echo "Ingrese la acción que desea realizar (CREATE,DESTROY,OUTPUT)"
read ACTION
echo "Ingrese la ruta completa en donde se encuentre el archivo de la cuenta de GCP (Ejm: /home/michael/challenge/task_1/account.json)"
read CREDENTIALS

if [ $ACTION == "CREATE" ] || [ $ACTION == "DESTROY" ]
then
    ansible-playbook main.yml --extra-vars project_id=$PROJECTID --extra-vars bash_action=$ACTION --extra-vars gcp_credentials=$CREDENTIALS --ask-become-pass
elif [ $ACTION == "OUTPUT" ]
then
    echo Output del Cluster
    terraform output ./files/Terraform/Cluster
    echo Output del Kubernetes
    terraform output ./files/Terraform/Kubernetes
else
    echo -n "Comando desconocido, actualmente solo se acepta: 'CREATE', 'DESTROY' u 'OUTPUT'"
fi