provider "google" {
  credentials = file(var.gcp_credentials)
  project     = var.gcp_project
  region      = var.gcp_region
}
provider "helm" {
  kubernetes {
    load_config_file       = false
    host     = google_container_cluster.main.endpoint

    username = var.gke_username
    password = var.gke_password
    insecure = true
  }
}