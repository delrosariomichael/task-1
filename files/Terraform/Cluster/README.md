 ## Providers

| Name | Version |
|------|---------|
| google | ~> 3.39.0 |
| helm | ~> 1.3.0 |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
 gcp\_credentials | Ruta de archivo de credenciales para la conexión a Google Cloud | `string` | n/a | yes |
| gcp\_project | Proyecto | `string` | n/a | yes |
| gcp\_region | Región | `string` | `"us-east1"` | no |
| gke\_name | Nombre del Kubernetes | `string` | `"kubernetes-michael"` | no |
| gke\_password | Contraseña para ingresar al Kubernetes | `string` | n/a | yes |
| gke\_username | Usuario para ingresar al Kubernetes | `string` | `"admin"` | no |
| gke\_zone | Zona donde se ubicará el Kubernetes | `string` | `"us-east1-b"` | no |

## Outputs

| Name | Description |
|------|-------------|
| gke\_host | Endpoint del Cluster |
| gke\_username | Usuario del Cluster |