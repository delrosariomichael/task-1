terraform {
  required_providers {
    kubernetes = {
      source  = "hashicorp/kubernetes"
      version = "~> 1.13.2"
    }
  }
}


resource "kubernetes_deployment" "main" {
  metadata {
    name = "python-app"
    labels = {
      test = "Terraform"
    }
  }

  spec {
    replicas = 1

    selector {
      match_labels = {
        test = "Terraform"
      }
    }

    template {
      metadata {
        labels = {
          test = "Terraform"
        }
      }

      spec {
        container {
          image = "gcr.io/${var.gcp_project}/michael.delrosario"
          name  = "python-app"
        }
      }
    }
  }
}

resource "kubernetes_service" "main" {
  metadata {
    name = "python-app-service"
  }
  spec {
    selector = {
      test = "Terraform"
    }
    port {
      port        = 80
      target_port = 80
    }

    type = "NodePort"
  }
}

resource "kubernetes_ingress" "main" {
  metadata {
    name = "python-app-ingress"
  }

  spec {
    backend {
      service_name = "python-app-service"
      service_port = 80
    }
  }
}