variable "gcp_project" {
  type = string
  description = "Proyecto"
}
variable "gke_password" {
  type = string
  description = "Contraseña para ingresar al Kubernetes"
}